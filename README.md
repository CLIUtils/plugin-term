# Demo of term


This serves as a demo of the features provided by [term](https://github.com/davidmogar/gitbook-plugin-term), and as a "test suite" of sorts. Check [Examples](examples.md) for more.

```
{% term lineStyles=true %} 
me@my-pc:~$ some --command "a" 'c'
term $ standard command
term$ another standard

term # root command
term# another root command
$ ./standard
# ./root
regular output
[warning]warning
[error]error
{% endterm %}
```

produces:

{% term lineStyles=true %} 
me@my-pc:~$ some --command "a" 'c'
term $ standard command
term$ another standard

term # root command
term# another root command
$ ./standard
# ./root
regular output
[warning]warning
[error]error
{% endterm %}

using the default settings.

This also augments the default highligher with a new language, `term`:

```term
me@my-pc:~$ some --command "a" 'c'
term $ standard command
term$ another standard

term # root command
term# another root command
$ ./standard
# ./root
regular output
[warning]warning
[error]error
```

This work inside blocks, but doesn't support other color styles very well or non-default options.

{% hint style="info" %}
In a block.

```term
term $ command
[warning]output
```

After a block.
{% endhint %}


Example for python:

```python
for i in range(1):
    print("Syntax Error")
```
