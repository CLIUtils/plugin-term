# term demos

The following code looks like this
```
{% term lineStyles=true %} 
me@my-pc:~$ some --command "a" 'c'
term $ standard command
term$ another standard

term # root command
term# another root command
$ ./standard
# ./root
regular output
[warning]warning
[error]error
{% endterm %}
```

The other styles add `style="stylename"`.

## Default

{% term lineStyles=true %} 
me@my-pc:~$ some --command "a" 'c'
term $ standard command
term$ another standard

term # root command
term# another root command
$ ./standard
# ./root
regular output
[warning]warning
[error]error
{% endterm %}

## Flat

{% term lineStyles=true, style="flat"%} 
me@my-pc:~$ some --command "a" 'c'
term $ standard command
term$ another standard

term # root command
term# another root command
$ ./standard
# ./root
regular output
[warning]warning
[error]error
{% endterm %}


## White

{% term style="white" %} 
me@my-pc:~$ some --command "a" 'c'
term $ standard command
term$ another standard

term # root command
term# another root command
$ ./standard
# ./root
regular output
[warning]warning
[error]error
{% endterm %}

## Ubuntu

{% term style="ubuntu" %} 
me@my-pc:~$ some --command "a" 'c'
term $ standard command
term$ another standard

term # root command
term# another root command
$ ./standard
# ./root
regular output
[warning]warning
[error]error
{% endterm %}

## Classic

{% term style="classic" %} 
me@my-pc:~$ some --command "a" 'c'
term $ standard command
term$ another standard

term # root command
term# another root command
$ ./standard
# ./root
regular output
[warning]warning
[error]error
{% endterm %}

## Black

{% term style="black" %} 
me@my-pc:~$ some --command "a" 'c'
term $ standard command
term$ another standard

term # root command
term# another root command
$ ./standard
# ./root
regular output
[warning]warning
[error]error
{% endterm %}

## Default again (should not be black)
{% term %}
me@my-pc:~# some --command "a" 'c'
{% endterm %}

## No copy buttons
{% term copyButtons=false %}
me@my-pc:~# some --command "a" 'c'
{% endterm %}

## No fade
{% term fade=false %}
me@my-pc:~# some --command "a" 'c'
{% endterm %}

# These are normal code blocks for comparison:

## Normal python 

```python
term $ if term
term$ term
term # term
term# term
$ term
# term
not term
```

## Normal plain

```
term $ term
term$ term
term # term
term# term
$ term
# term
not term
```

## Broken use of shortcut

For some reason, using a triple-tickmark+output shortcut converts to tags, but then does not get processed by the block system.

```output
me@my-pc:~$ some --command "a" 'c'
term $ term
term$ term
term # term
term# term
$ term
# term
not term
```

